'use strict';

var app = angular.module('gotoco.controllers');

app.controller('SessionCtrl', function ($scope, $stateParams, $log, $window, CacheService, PersistenceService) {
  var sessionId = $stateParams.sessionId;
  $log.debug('State params: ' + sessionId);
  $scope.tracks = [];
  $scope.isOnMySchedule = false;

  $scope.openExternal = function (title) {
    $window.open('http://gotocon.com/berlin-2014/presentation/' + title, '_system', 'location=yes');
  };

  $scope.addSession = function (id) {
    PersistenceService.addSession({id: id}).then(function () {
      $scope.isOnMySchedule = true;
    });
  };

  $scope.removeSession = function (id) {
    PersistenceService.removeSession(id).then(function () {
      $scope.isOnMySchedule = false;
    });
  };


  CacheService.getSession({sessionId: sessionId}).$promise
    .then(function (data) {
      $log.debug('Ctrl got data');
      $scope.session = data;

      $scope.rewardable = $scope.session.timeSlots.filter(function (ts) {
        return ts.sessionInfo.countsForReward;
      }).length > 0;

      return data;
    })
    .then(function (session) {
      var trackIds = session.trackIds;
      angular.forEach(trackIds, function (trackId) {
        CacheService.getTrack({trackId: trackId}).$promise
          .then(function (track) {
            $scope.tracks.push(track);
          })
          .catch(function (err) {
            $log.error('Failed !' + err);
          });
      });
      return session;
    })
    .then(function (session) {
      PersistenceService.getSessionFromMySchedule(session.id)
        .then(function (data) {
          $scope.isOnMySchedule = typeof(data) !== 'undefined';
        });
    })
    .catch(function (err) {
      $log.error('Failed !' + err);
    });
});
