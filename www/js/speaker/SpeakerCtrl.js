/*
 * (C) 2014 Michael Engelhardt <me@mindcrime-ilab.de>
 *
 * This file is part of Goto.Co.
 *
 * Goto.Co is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Goto.Co is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Goto.Co.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

var app = angular.module('gotoco.controllers');

app.controller('SpeakerCtrl', function ($scope, $stateParams, $log, CacheService, $window) {
  var speakerId = $stateParams.speakerId;
  $log.debug('State params: ' + speakerId);


  CacheService.getSpeaker({speakerId: speakerId}).$promise
    .then(function (data) {
      $log.debug('Ctrl got data');
      $scope.speaker = data;

      return data;
    })
    .catch(function (err) {
      $log.error('Failed !' + err);
    });

  $scope.openTwitter = function(twitterId) {
    $window.open('https://twitter.com/'+twitterId, '_system', 'location=yes');
  }
});
