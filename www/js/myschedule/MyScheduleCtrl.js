'use strict';

var app = angular.module('gotoco.controllers');

app.controller('MyScheduleCtrl', ['$scope', '$log','PersistenceService', 'CacheService', function ($scope, $log, PersistenceService, CacheService) {
  $log.debug('My Sessions List');
  $scope.sessions = [];

  PersistenceService.getMySchedule()
    .then(function(ids){
      $log.debug('Ids: '+ids);
      angular.forEach(ids, function(id){
        CacheService.getSession({sessionId:id}).$promise
          .then(function(session){
            $scope.sessions.push(session);
          });
      });
    })
    .catch(function(err){
      $log.error('Failed !' + err);
    });


}]);
