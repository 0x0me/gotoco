/*
 * (C) 2014 Michael Engelhardt <me@mindcrime-ilab.de>
 *
 * This file is part of Goto.Co.
 *
 * Goto.Co is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Goto.Co is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Goto.Co.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

var app = angular.module('gotoco.controllers');

app.controller('TracksCtrl', function ($scope, $log, $ionicLoading, CacheService) {
  $log.debug('Tracks Controller');

  function load() {

    $log.debug('Showing progress img');
    $ionicLoading.show({
      template: '<div class="loader"><i class="icon ion-loading-c"></i><br /><span>Loading...</span></div>'
    });

    $scope.tracks  = [];

    CacheService.queryForTracks().$promise
      .then(function (data) {

        var tracks = data.result.map(function (track) {
          return track.id;
        });


        $log.debug('Number of tracks: ' + tracks.length);

        angular.forEach(tracks, function (track, idx) {
          if ((idx + 1) === tracks.length) {
            loadTrack(track, function () {
              $ionicLoading.hide();
              $log.debug('progress screen hidden');
            });
          }
          else {
            loadTrack(track);
          }
        });
      })
      .catch(function (err) {
        $log.error('Could not fetch track data: ' + err);
      });
  }

  function loadTrack(id, func) {
    CacheService.getTrack({trackId: id}).$promise
      .then(function (data) {
        $scope.tracks.push(data);
      })
      .catch(function (err) {
        $log.error('Could not fetch detail track information' + err);
      })
      .finally(function () {
        if (typeof(func) === 'function') {
          func();
        }
      });
  }

  if (typeof($scope.tracks) === 'undefined') {
    load();
  }

});
