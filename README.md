#Goto.Co - Goto Berlin Conference Companion

##About 
Goto.Co is a fast and small hack building my personal conference planner for the 
[Goto Conference](http://www.gotocon.com/berlin-2014) in Berlin.

##Download
Download the latest development version from [https://bitbucket.org/0x0me/gotoco/downloads/](https://bitbucket.org/0x0me/gotoco/downloads/GotoCoApp-debug.apk) 
or try online version from [http://apps.mindcrime-ilab.de/gotoco](http://apps.mindcrime-ilab.de/gotoco). The Android version requires at least Android version 4.4.

###Changes

* Build 10 fixes the nasty bug of doubling entries in my schedule and track view

##Development
Goto.Co is based on the [ionic framework](http://www.ionicframework.com) which faciliates [Apache Cordova](https://cordova.apache.org)
 and [AngularJS](https://angularjs.org).

##License
Goto.Co is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as 
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Goto.Co is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Goto.Co. If not, see 
[http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).